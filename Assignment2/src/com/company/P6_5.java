package com.company;

import java.util.Scanner;
public class P6_5 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter a number: ");
        int num = scan.nextInt();

        PrimeGenerator generator= new PrimeGenerator(num);
        //System.out.println("");
        int output = generator.nextPrime();
        while(output!=-1){
            System.out.println(output+ " ");
            output = generator.nextPrime();
        }
        //System.out.println(generator.nextPrime(num));

    }

}
