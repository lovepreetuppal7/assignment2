package com.company;

public class PrimeGenerator {
    private int num;
    private int i;
    public PrimeGenerator(int num){
        this.num = num;
        i =2;

    }

    public static boolean isPrime(int num){
        if(num<2){
            return false;
        }
        for(int i = 2;i<num;i++){
            if(num%i == 0) {
                return false;
            }
        }
        return true;
    }

    public int nextPrime() {

        if(i <= num) {
            while (!isPrime(i)) {
                i = i + 1;
            }
            int result = i;
            i = i + 1;
            if(result<=num){
                return result;
            }
            else{
                return -1;
            }
        }
        else{
            return -1;
        }
    }


}



