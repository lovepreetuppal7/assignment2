package com.company;

import java.util.Scanner;

public class E4_17 {
    public static void main(String[] args){
        int firstTime;
        int secondTime;
        Scanner scan = new Scanner(System.in);
        System.out.println("Please enter the first time: ");
        firstTime = scan.nextInt();
        System.out.println("Please enter the second time: ");
        secondTime = scan.nextInt();

        int firstHour = firstTime/100;
        int firstMinute = firstTime%100;

        int secondHour = secondTime/100;
        int secondMinute = secondTime%100;
        if(secondTime > firstTime){
            System.out.println((secondHour - firstHour) + " Hours "+ (secondMinute - firstMinute)+ " Minutes ");
        }
        else {
            secondTime = secondTime + 2360;
            secondHour = secondTime/100;
            secondMinute = secondTime%100;
            System.out.println((secondHour - firstHour) + " Hours "+ (secondMinute - firstMinute)+ " Minutes ");

        }
    }
}
